public class ExPalindromos {
    public static void main(String[] args){
        TesteProblemasComStrings();
    }

    private static void TesteProblemasComStrings(){
        System.out.println("exemplo Esperado: É Palindromo, resultado: "+IdentificarPalindromo("OMISSISSIMO"));
        System.out.println("exemplo Esperado: Não é Palindromo, resultado: "+IdentificarPalindromo("AÇUCAR"));

        System.out.println("Esperado: É Palindromo, resultado: "+IdentificarPalindromoBlankSpace("ROMA ME TEM AMOR"));
    }
    private static String IdentificarPalindromo(String s){
        String inverso = new StringBuilder(s).reverse().toString();
        return (inverso.toLowerCase().equals(s.toLowerCase())) ? "É Palindromo" : "Não é Palindromo";
    }

    private static String IdentificarPalindromoBlankSpace(String s){
        s = s.replaceAll("\\s" , "");
        String inverso = new StringBuilder(s).reverse().toString();
        return (inverso.toLowerCase().equals(s.toLowerCase())) ? "É Palindromo" : "Não é Palindromo";
    }
}
