public class ExArranjos {
    public static void main(String[] args){
        TesteArranjos();

    }
    private static void TesteArranjos(){
        int[] arranjoExemplo = {10,20,30,35,5};
        System.out.println("Exemplo Esperado: 100, Resultado: "+SomaArranjo(arranjoExemplo) + " usando  for()");
        System.out.println("Exemplo Esperado: 100, Resultado: "+SomaArranjoR(arranjoExemplo, arranjoExemplo.length) + " metodo Recursivo");

        System.out.println("Exemplo Esperado: 65, Resultado: "+SomaMaioresElementosDoArranjo(arranjoExemplo) + " usando for()");
    }
    private static int SomaArranjo(int[] array){
             int soma = 0;
        for( int i = 0; i<array.length; i++){
                 soma += array[i];
        }
        return soma;
    }

    private static int SomaMaioresElementosDoArranjo(int[] array){

    int[] MaioresElemento = {0,0};
        for( int i = 0; i<array.length; i++){
         if( MaioresElemento[0] <  array[i]){
             MaioresElemento[1] = MaioresElemento[0];
             MaioresElemento[0] = array[i];
         }
        }
        return MaioresElemento[0] + MaioresElemento[1];
    }

    private static int SomaArranjoR(int[] array, int tamArray){
        if( tamArray>0){
            int soma = array[tamArray - 1] + SomaArranjoR(array, tamArray - 1);

            return soma;
        } else {
            return  0;
        }
    }
}
