import java.util.*;
public class ExFuncoes {
    //Conjunto no Void Main
    public static void main(String[] args){
        List<String> conjuntoPalavras = new ArrayList<String>();
        conjuntoPalavras.add("radar");
        conjuntoPalavras.add("rever");
        conjuntoPalavras.add("rachar");
        conjuntoPalavras.add("analise");
        conjuntoPalavras.add("interpretacao");
        conjuntoPalavras.add("a mae te ama");

        TestePalindromos(conjuntoPalavras);
    }

    private static void TestePalindromos(List<String> conjunto){
        System.out.println("Resultado: "+PalindromosNoConjunto(conjunto));

        System.out.println("Resultado: "+DigitosPalindromos(10000));
    }

    private static String PalindromosNoConjunto(List<String> conjunto) {
        String strWBlankSpace = "";
        String fullString = "";
        for (int s = 0; s < conjunto.size(); s++) {
            strWBlankSpace = conjunto.get(s).replaceAll("\\s", "");
            String inverso = new StringBuilder(strWBlankSpace).reverse().toString();
            if (inverso.toLowerCase().equals(strWBlankSpace.toLowerCase())) {
                fullString += conjunto.get(s) + ", ";
            }
        }
        return fullString;
    }

    private static String DigitosPalindromos(int max){
    String digitos="";
     for(int i = 0; i<= max; i++){
         if( i>10) {
             String digitoInverso = new StringBuilder(String.valueOf(i)).reverse().toString();
             if( digitoInverso.equals(String.valueOf(i))) {
                 digitos += digitoInverso + ", ";
             }
         }else{
             String digitoInverso = new StringBuilder(String.format("%02d", i)).reverse().toString();
             if( digitoInverso.equals(String.format("%02d", i))) {
                 digitos += digitoInverso + ", ";
             }
         }
     }
    return digitos;
    }
}
