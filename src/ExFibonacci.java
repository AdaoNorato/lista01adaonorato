public class ExFibonacci {
    public static void main(String[] args){
        TesteFibonacci();
    }

    private static void TesteFibonacci(){
    System.out.println("Esperado 55, metodo recursivo, resultado: " + FibR(10));
    System.out.println("Esperado 55, metodo usando While, resultado: " + Fib(10));
    }

    private static int FibR(int n){
        return   (n<3) ? 1 : FibR(n-1) + FibR(n-2);
    }

    private static int Fib(int n){
        int i=0, fibA=0, fibB=1;
        while( i < n-1){
               int soma = fibA + fibB;
               fibA = fibB;
               fibB = soma;
               i++;
        }
        return fibB;
    }
}
