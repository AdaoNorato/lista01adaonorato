public class ExStrings {
    public static void main(String[] args){
        TesteProblemasComStrings();
    }
    private static void TesteProblemasComStrings(){
        String texto = "Olá, mundo, brabo!";
        char caractere = ',';

        System.out.println("frase após caractere: "+FraseAposCaractere(texto,caractere));
        System.out.println("frase após ultimo caractere: "+FraseAposUltCaractere(texto,caractere));
        System.out.println("frase entre caracteres: "+FraseEntreCaracteres(texto,caractere));
        System.out.println("frase após segundo caractere: "+FraseAposSegCaractere(texto,caractere));
    }

    private static String FraseAposCaractere(String s , char c){
         String texto="";
         for(int i = 0; i < s.length(); i++){
            texto += s.charAt(i);
            if( s.charAt(i) == c) {
                texto = s.replace(texto,"");
                i = s.length();
            }
          }
     return texto;
    }

    private static String FraseAposUltCaractere(String s , char c){
        String texto="";
        for(int i = 0; i < s.length(); i++){
            texto += s.charAt(i);
            if( s.charAt(i) == c){
                texto = "";
            }
        }
        return texto;
    }

    private static String FraseEntreCaracteres(String s , char c){
        String texto= s;
        texto = texto.substring(texto.indexOf(',')+1, texto.lastIndexOf(','));

        return texto;
    }

    private static String FraseAposSegCaractere(String s , char c){
        String texto="";
        int index = 1;

        for( int i = 0; i < s.length(); i++){
            texto += s.charAt(i);
            if( s.charAt(i) == c){
                index++;
                if( index > 2){
                    texto = s.replace(texto, "");
                    i = s.length();
                }
            }
        }
        return texto;
        }
}

